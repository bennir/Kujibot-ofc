# Kujibot
version = "1.4.0"
update_script = ['nohup', '/home/luxbot/bot_update.sh']

# BDO
classes = ["warrior", "sorceress", "ranger", "berserker", "valkyrie", "musa", "maehwa", "wizard", "witch", "kunoichi", "ninja", "striker", "mystic", "tamer", "darkknight", "lahn"]
enhance_levels = ["low", "PRI", "DUO", "TRI", "TET", "PEN"]

# Discord Server
role_member = "BDO Members"
role_admin = "BDO Officers"
role_commander = "BDO Commanders"
delete_msgs = False
status_channelid = "426659687006470144"
main_channelid = "426660359974289418"
mute = False

# Node War Event
node_war_description = "Meetup on Med 6 and voice at 19:30 cet, bring villa buffs and elixirs"
notification_channelid = "365907474278121472"
signup_cutoff_time = 60 #in minutes
clear_event_check = "Pa$$w0rd"

# War Sheet
war_sheet_name = 'Setup'
war_sheet_start_column = 'A'
war_sheet_start_row = 2
war_sheet_end_column = 'G'
war_sheet_end_row = 100
war_sheet_d_column_formula = "=IFERROR(VLOOKUP(AXXX,Roster!$B$2:$R$101,8,FALSE))"

# Member Sheet
member_sheet_name = 'Members'
member_sheet_character = 'B'
member_sheet_class ='C'
member_sheet_lvl = 'D'
member_sheet_discord = 'F'
member_sheet_discordid = 'H'
member_sheet_gear = 'K'
member_sheet_axe = 'L'
