import discord
from discord.ext.commands import Bot
from discord.ext import commands

from pony.orm import db_session, commit, select, avg, count, delete

from Database import *
import static

class Dbadmin(object):
    def __init__(self, bot):
        self.bot = bot    

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def dbclearevents(self, ctx, *args):
        if not args or args[0] != static.clear_event_check:
            await self.bot.say("{}, please use the password to clear all events: {}".format(ctx.message.author.mention, static.clear_event_check))
            return
        
        if args[0] == static.clear_event_check:
            with db_session:
                delete(e for e in Event)
                commit()
            await self.bot.say("{}, all Events have been deleted.".format(ctx.message.author.mention))
        

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def dblistevents(self, ctx):
        with db_session:
            events = select(e for e in Event)
            for event in events:
                response = "ID: {} - Time: {} - Updated: {} - Title: {} - Description: {} - Creator: {}" \
                            .format(event.id, event.time, event.updated, event.title, event.description, event.creator)
                await self.bot.send_message(ctx.message.author, response)


    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def dblistusers(self, ctx):
        with db_session:
            users = select(u for u in User)
            for user in users:
                response = "Discord_id: {} - family: {} - ap: {} - awap: {} - dp: {} - lvl: {} - character: {} - class: {} - axe: {}" \
                            .format(user.discord_id, user.family, user.ap, user.awap, user.dp, user.lvl, user.character, user.gameclass, user.axe)
                await self.bot.send_message(ctx.message.author, response)


    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def deluser(self, ctx, user_id:str):
        with db_session:
            user = User.get(discord_id=user_id)
            await self.bot.say("{}, User {} has been deleted.".format(ctx.message.author.mention, user.discord_id))
            user.delete()
            commit()


    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def delevent(self, ctx, event_id:int):
        with db_session:
            event = Event.get(id=event_id)
            await self.bot.say("{}, Event {} has been deleted.".format(ctx.message.author.mention, event.id))
            event.delete()
            commit()

    

def setup(bot):
    bot.add_cog(Dbadmin(bot))