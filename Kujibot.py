import static
import math
import asyncio
import subprocess
from subprocess import Popen
from pony.orm import db_session, commit, select, avg, count, delete, sum, desc
from datetime import datetime, timedelta

import discord
from discord.ext.commands import Bot
from discord.ext import commands

import secret
from Database import *

class Kujibot(object):

    def __init__(self, bot):
        self.bot = bot
        
            
    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def addevent(self, ctx, date:str, time:str, event:str, description:str):
        with db_session:
            author = get_user(ctx.message.author.id)

            if not author.character:
                if not static.mute:
                    await self.bot.say("{}, you need to add a character first.".format(ctx.message.author.mention))
                return
                
            ev = Event(time=datetime.strptime(date + time, "%d.%m.%Y%H:%M"),
                       updated=datetime.now(),
                       title=event,
                       description=description,
                       creator=author)
            commit()

            if not static.mute:
                await self.bot.say("{}, added your event on {} at {} with ID {}: {}".format(ctx.message.author.mention, date, time, ev.id, event))
        if static.delete_msgs:
            await self.bot.delete_message(ctx.message)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def addnw(self, ctx, date:str, time:str):
        with db_session:
            author = get_user(ctx.message.author.id)

            if not author.character:
                if not static.mute:
                    await self.bot.say("{}, you need to add a character first.".format(ctx.message.author.mention))
                return
                
            ev = Event(time=datetime.strptime(date + time, "%d.%m.%Y%H:%M"),
                       updated=datetime.now(),
                       title="Node War",
                       description=static.node_war_description,
                       creator=author)
            commit()

            if not static.mute:
                await self.bot.say("{}, added nodewar on {}, {} CET with ID {}".format(ctx.message.author.mention, date, time, ev.id))
        if static.delete_msgs:
            await self.bot.delete_message(ctx.message)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def edittitle(self, ctx, event_id:int, title:str):
        with db_session:
            event = Event.get(id=event_id)
            if not event:
                await self.bot.say("{}, Event {} does not exist.".format(ctx.message.author.mention, event_id))
                return
            if not title:
                await self.bot.say("{}, please use -edittitle EventId \"Title\" .".format(ctx.message.author.mention))

            event.title = title
            commit()
            await self.bot.say("{}, Event {} has been edited.".format(ctx.message.author.mention, event.id))

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def editdesc(self, ctx, event_id:int, description:str):
        with db_session:
            event = Event.get(id=event_id)
            if not event:
                await self.bot.say("{}, Event {} does not exist.".format(ctx.message.author.mention, event_id))
                return
            if not description:
                await self.bot.say("{}, please use -editdesc EventId \"Description\" .".format(ctx.message.author.mention))

            event.description = description
            commit()
            await self.bot.say("{}, Event {} has been edited.".format(ctx.message.author.mention, event.id))
            

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_member)
    async def list(self, ctx):
        embed = discord.Embed(title="Event list", description="", color=0xffff00)
        with db_session:
            events = select(e for e in Event if e.time > datetime.now()).order_by(Event.time)[:5]
        for event in events:
            embed.add_field( \
                name="{} - {} - {} CET ({})".format(event.id, event.title, event.time.strftime('%d.%m.%Y %H:%M'), event.time.strftime('%A')), \
                value="{}".format(event.description), inline=False \
            )

        if not static.mute:
            await self.bot.send_message(ctx.message.channel, embed=embed)
        if static.delete_msgs:
            await self.bot.delete_message(ctx.message)
            

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_member)
    async def history(self, ctx, *args):
        days = 30
        if len(args):
            days = int(args[0])
        
        embed = discord.Embed(title="Signed up last {} days".format(days), description="", color=0xffff00)            
        author = get_user(ctx.message.author.id)
        with db_session:
            events = select(e for e in Event 
                                if author in e.signups 
                                and (e.time >= datetime.now() - timedelta(days=days))
                            ).order_by(desc(Event.time))[:]
        for event in events:
            embed.add_field( \
                name="{} - {} - {} CET ({})".format(event.id, event.title, event.time.strftime('%d.%m.%Y %H:%M'), event.time.strftime('%A')), \
                value="{}".format(event.description), inline=False \
            )

        if not static.mute:
            await self.bot.send_message(ctx.message.channel, embed=embed)
        if static.delete_msgs:
            await self.bot.delete_message(ctx.message)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_member)
    async def signup(self, ctx, event_id:int, *args):
        with db_session:
            event = Event.get(id=event_id)
            author = get_user(ctx.message.author.id)

            if not author:
                if not static.mute:
                    await self.bot.say("{}, please set your family first.".format(ctx.message.author.mention))
                return
            if not author.character:
                if not static.mute:
                    await self.bot.say("{}, you need to add a character first.".format(ctx.message.author.mention))
                return
            if not event:
                if not static.mute:
                    await self.bot.say("{}, that event does not exist.".format(ctx.message.author.mention))
                return
            elif event.time < datetime.now():
                if not static.mute:
                    await self.bot.say("{}, that event is already over.".format(ctx.message.author.mention))
                return 
            elif (event.time - datetime.now()) < timedelta(minutes=static.signup_cutoff_time):
                channel = self.bot.get_channel(secret.channel_officer) 

                if len(args) and args[0] == "late":
                    if not static.mute:
                        await self.bot.say("{}, thanks for notifying us, the message has been relayed to the officers!".format(ctx.message.author.mention))
                    
                    event.late.add(author)
                    await self.bot.send_message(channel, "{} will be late for {}!".format(ctx.message.author.name, event.id))
                else:
                    if not static.mute:
                        await self.bot.say("{}, signups for this event are closed, officers have been notified.".format(ctx.message.author.mention))
                        
                    await self.bot.send_message(channel, "{} tried to signup for event {} after the time.".format(ctx.message.author.name, event.id))
                return            
            elif author in event.signups:
                if author in event.late and len(args) == 0:
                    event.late.remove(author)
                    if not static.mute:
                        await self.bot.say("{}, glad you can make it on time.".format(ctx.message.author.mention))
                elif author not in event.late and len(args) and args[0] == "late":
                    event.late.add(author)
                    if not static.mute:
                        await self.bot.say("{}, sad to see you will be late.".format(ctx.message.author.mention))
                else:
                    if not static.mute:
                        await self.bot.say("{}, you're already signed up for this event.".format(ctx.message.author.mention))
                    return

            if len(args) and args[0] == "late":
                event.late.add(author)
                
            if author in event.signouts:
                event.signouts.remove(author)

            event.signups.add(author)
            commit()

            if not static.mute:
                await self.bot.say("{}, I've signed you up for the event.".format(ctx.message.author.mention))
                
            await self.update_display(event_id)
        if static.delete_msgs:
            await self.bot.delete_message(ctx.message)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_member)
    async def signout(self, ctx, event_id:int):
        with db_session:
            event = Event.get(id=event_id)
            author = get_user(ctx.message.author.id)

            if not author:
                if not static.mute:
                    await self.bot.say("{}, please set your family first.".format(ctx.message.author.mention))
                return
            if not author.character:
                if not static.mute:
                    await self.bot.say("{}, you need to add a character first.".format(ctx.message.author.mention))
                return
            if not event:
                if not static.mute:
                    await self.bot.say("{}, that event does not exist.".format(ctx.message.author.mention))
                return
            elif event.time < datetime.now():
                if not static.mute:
                    await self.bot.say("{}, that event is already over.".format(ctx.message.author.mention))
                return
            elif (event.time - datetime.now()) < timedelta(minutes=static.signup_cutoff_time):
                channel = self.bot.get_channel(secret.channel_officer) 
                await self.bot.send_message(channel, "{} signed out of event {} after the time.".format(ctx.message.author.name, event.id))
                if not static.mute:
                    await self.bot.say("{}, thanks for letting us know you are not coming, officers have been notified!".format(ctx.message.author.mention))     
                if author in event.signups:
                    event.signups.remove(author)                
                if author in event.late:
                    event.late.remove(author)

                event.signouts.add(author)
                commit()
                await self.update_display(event_id)
                if static.delete_msgs:
                    await self.bot.delete_message(ctx.message)
                return  

            if author in event.signups:
                event.signups.remove(author)
                
            if author in event.late:
                event.late.remove(author)
                
            event.signouts.add(author)
            commit()

            if not static.mute:
                await self.bot.say("{}, I've signed you out for the event.".format(ctx.message.author.mention))

            await self.update_display(event_id)
        if static.delete_msgs:
            await self.bot.delete_message(ctx.message)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def signups(self, ctx, event_id:int, *args):
        with db_session:
            event = Event.get(id=event_id)
            if not event:
                if not static.mute:
                    await self.bot.say("{}, that event does not exist.".format(ctx.message.author.mention))
                return
            if len(args) and (args[0] == "full" or args[0] == "family"):
                signups_msg = "{}, Event ({}) {} - {}\n{} signups and {} signouts.\n\n"
                signups_msg = signups_msg.format(ctx.message.author.mention, event.id, event.title, event.time, len(event.signups), len(event.signouts))
                
                if not static.mute:
                    await self.bot.say(signups_msg)

                if len(event.signups):
                    msg = "{} people signed up:\n```md\n".format(len(event.signups))

                    if args[0] == "family":
                        signups = select(u for u in User if u in event.signups).order_by(User.family)  
                    else:   
                        signups = select(u for u in User if u in event.signups).order_by(User.character)    
                    for user in signups:
                        if args[0] == "family":
                            msg = msg + "{}[{}]({}) {}\n".format(user.family, user.character, user.gameclass.capitalize(), "\t**LATE**" if user in event.late else "")
                        else:
                            msg = msg + "[{}]({})<{}><{}/{}>{}\n".format(user.character, user.gameclass.capitalize(), user.lvl, (user.ap + user.awap) / 2 + user.dp, user.axe, "\t**LATE**" if user in event.late else "")

                    msg = msg + "\n```\n"
                    
                    if not static.mute:
                        await self.bot.say(msg)     

                if len(event.signouts):
                    msg = "{} people signed out:\n```md\n".format(len(event.signouts))

                    if args[0] == "family":
                        signouts = select(u for u in User if u in event.signouts).order_by(User.family) 
                    else:   
                        signouts = select(u for u in User if u in event.signouts).order_by(User.character)    
                    for user in signouts:
                        if args[0] == "family":
                            msg = msg + "{}[{}]({}) {}\n".format(user.family, user.character, user.gameclass.capitalize(), "\t**LATE**" if user in event.late else "")
                        else:
                            msg = msg + "[{}]({})<{}><{}/{}>{}\n".format(user.character, user.gameclass.capitalize(), user.lvl, (user.ap + user.awap) / 2 + user.dp, user.axe, "\t**LATE**" if user in event.late else "")

                    msg = msg + "\n```\n"
                    
                    if not static.mute:
                        await self.bot.say(msg)   

            else:
                if not static.mute:
                    await self.bot.say("{}, Event ({}) {} - {}\n{} signups and {} signouts.".format(ctx.message.author.mention, event.id, event.title, event.time, len(event.signups), len(event.signouts)))
        if static.delete_msgs:
            await self.bot.delete_message(ctx.message)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_member)
    async def allsignups(self, ctx, *args):
        with db_session:
            if not args:
                user_id = ctx.message.author.id
                msg = "{}, you have ".format(ctx.message.author.mention)
            else:
                user_id = args[0]
                msg = "{}, user {} has ".format(ctx.message.author.mention, user_id)

            user = get_user(str(user_id))
            
            signups = select(e for e in Event if user in e.signups and e.time > datetime.now())            
            signouts = select(e for e in Event if user in e.signouts and e.time > datetime.now())

            if len(signups) == 0 and len(signouts) == 0:
                if not static.mute:
                    if not args:
                        await self.bot.say("{}, you have not signed in or out of any event.".format(ctx.message.author.mention))
                    else:
                        await self.bot.say("{}, user {} has not signed in or out of any event.".format(ctx.message.author.mention, user_id))
                return

            if len(signups) == 0:
                msg = msg + "not signed in to any event "
            else:
                msg = msg + "signed in to {} event{} ".format(len(signups), "s" if len(signups) > 1 else "")

            if len(signouts) == 0:
                msg = msg + "and not signed out of any event."
            else:
                msg = msg + "and signed out of {} event{} ".format(len(signouts), "s" if len(signouts) > 1 else "")

            msg = msg + "\n"
            
            if len(signups) > 0:
                msg = msg + "Signups:\n```md\n"
                for event in signups:
                    msg = msg + "{}: {} - {}\n".format(event.id, event.title, event.time)
                msg = msg + "```"
            
            if len(signouts) > 0:
                msg = msg + "\n\nSignouts:\n```md\n"
                for event in signouts:
                    msg = msg + "{}: {} - {}\n".format(event.id, event.title, event.time)
                msg = msg + "```"
            
            if not static.mute:
                await self.bot.say(msg)
            

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def non_signed(self, ctx, event_id:int, *args):
        with db_session:
            event = Event.get(id=event_id)
            users = select(u for u in User)
            if not event:
                if not static.mute:
                    await self.bot.say("{}, that event does not exist.".format(ctx.message.author.mention))
                return

            if len(users) == (len(event.signups) + len(event.signouts)):
                msg = "{}, Event ({}) - {} - {}\n{} signups and {} signouts. Everyone used the bot!"
                msg = msg.format(ctx.message.author.mention, event.id, event.title, event.time, len(event.signups), len(event.signouts))
                if not static.mute:
                    await self.bot.say(msg)
                return
            
            msg = "{}, Event ({}) - {} - {}\n{} signups and {} signouts. These people did **not** sign:\n"
            msg = msg.format(ctx.message.author.mention, event.id, event.title, event.time, len(event.signups), len(event.signouts))
            
            if len(args) and args[0] == "ping":
                msg = msg + " ".join("<@{}>".format(user.discord_id) for user in users if user not in (event.signups) and user not in (event.signouts)) + "\n"
            else:
                for user in users:
                    if (user not in (event.signups)) and (user not in (event.signouts)):
                        u = await discord.Client.get_user_info(self.bot, user.discord_id)
                        msg = msg + "{}\n".format(u.display_name)

            if not static.mute:
                await self.bot.say(msg)
        if static.delete_msgs:
            await self.bot.delete_message(ctx.message)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_member)
    async def user(self, ctx, *args):
        msg = "```md\n" \
                "[{}]({})<{}>\n" \
                "<Lvl> <AP> <AW> <DP> <GS> <Axe>\n" \
                " {}    {}  {}  {}  {}  {}\n" \
                 "```"
        warning = "{}, you don't have a character set up yet.\n" \
                    "Please use {}character for that".format(ctx.message.author.mention, secret.COMMAND_PREFIX)

        if not args:
            user = get_user(ctx.message.author.id)
            if (user.family == "" and not static.mute):
                await self.bot.say("{}, please set a family name first using {}family.".format(ctx.message.author.mention, secret.COMMAND_PREFIX))
                return
            if (user.character == "" and not static.mute):
                await self.bot.say(warning)
            else:
                msg = msg.format(user.character, user.gameclass.capitalize(), user.family, user.lvl, user.ap, user.awap, user.dp, math.floor((user.ap + user.awap) / 2 + user.dp), user.axe)
                if not static.mute:
                    await self.bot.say(msg)
        elif len(args):
            user = get_user(args[0])
            if (not user and not static.mute):
                    await self.bot.say("{}, that user has no character.".format(ctx.message.author.mention))
            else:
                msg = msg.format(user.character, user.gameclass.capitalize(), user.family, user.lvl, user.ap, user.awap, user.dp, math.floor((user.ap + user.awap) / 2 + user.dp), user.axe)
                if not static.mute:
                    await self.bot.say(msg)
        if static.delete_msgs:
            await self.bot.delete_message(ctx.message)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_member)
    async def family(self, ctx, family:str):  
        if family:
            with db_session:
                user = get_user_or_create(ctx.message.author.id)
                user.family = family

                commit()
                if not static.mute:
                    await self.bot.say("{}, your family name has been set.".format(ctx.message.author.mention))

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def editfamily(self, ctx, discord_id:str, family:str):  
        if family:
            with db_session:
                user = get_user(discord_id)
                user.family = family

                commit()
                if not static.mute:
                    await self.bot.say("{}, family has been set.".format(ctx.message.author.mention))


    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_member)
    async def gear(self, ctx):
        with db_session:
            avg_ap = avg(u.ap for u in User if u.ap > 0)
            avg_awap = avg(u.awap for u in User if u.awap > 0)
            avg_dp = avg(u.dp for u in User if u.dp > 0)
            avg_gs = (avg_ap + avg_awap) / 2. + avg_dp
            axes = count(u for u in User if u.axe and u.axe not in static.enhance_levels[0:2])

            char_count = count(u for u in User if u.lvl > 0)
            sum_lvl = sum(u.lvl for u in User if u.lvl > 0)
            avg_lvl = 1.0*sum_lvl / char_count

            msg = "```md\n" \
                  "[Average Stats]\n" \
                  "<Lvl>  <AP> <AW> <DP> <GS> <Axes>\n" \
                  " {0:0.2f}  ".format(avg_lvl)
            msg = msg + "{}  {}  {}  {}  {}\n" \
                  "```"
            msg = msg.format(math.floor(avg_ap), math.floor(avg_awap), math.floor(avg_dp), math.floor(avg_gs), axes)
            if not static.mute:
                await self.bot.say(msg)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def display(self, ctx, event_id:int):
        with db_session:
            delete(d for d in Display)

        try:
            channel = self.bot.get_channel(secret.channel_status) 
            async for x in self.bot.logs_from(channel):
                await self.bot.delete_message(x)
                await asyncio.sleep(0.3)
        except:
            print('Message to delete not found')
            
        await self.update_display(event_id)

    async def update_display(self, event_id):
        with db_session:
            event = Event.get(id=event_id)
            if event and event.time > datetime.now():                
                channel = self.bot.get_channel(secret.channel_status) 

                displays = select(d for d in Display)
                display_event_id = -1
                for display in displays:
                    display_event_id = display.event_id

                if len(displays) == 0 or event_id == display_event_id:
                    try:
                        for display in displays:
                            message = await self.bot.get_message(channel, display.message_id)
                            await self.bot.delete_message(message)
                    except:
                        print('Message from db did not exist anymore')

                    delete(d for d in Display)

                    display_msg = "Event ({}) {} - {}\n".format(event.id, event.title, event.time)
                    display_msg = display_msg + "{}\n\n".format(event.description)
                    
                    non_signed = count(u for u in User if u not in event.signups and u not in event.signouts)
                    if non_signed == 0:
                        display_msg = display_msg + "Everyone signed up, hooray!\n\n"
                    elif non_signed == 1:
                        display_msg = display_msg + "{} person did not sign yet.\n\n".format(non_signed)
                    else:
                        display_msg = display_msg + "{} people did not sign yet.\n\n".format(non_signed)

                    message = await self.bot.send_message(channel, display_msg)
                    print('Display added: ' + message.id)
                    display = Display(event_id=event_id, message_id=message.id)
                    commit()
                    
                    if len(event.signups):
                        msg = "{} people signed up:\n```md\n".format(len(event.signups))
                        
                        signups = select(u for u in User if u in event.signups).order_by(User.character)    
                        for user in signups:
                            msg = msg + "[{}]({})<{}><{}/{}>{}\n".format(user.character, user.gameclass.capitalize(), user.lvl, (user.ap + user.awap) / 2 + user.dp, user.axe, "\t**LATE**" if user in event.late else "")
                        msg = msg + "\n```\n"

                        message = await self.bot.send_message(channel, msg)
                        print('Display added: ' + message.id)
                        display = Display(event_id=event_id, message_id=message.id)
                        commit()


                    if len(event.signouts):
                        msg = "{} people signed out:\n```md\n".format(len(event.signouts))
                        
                        signouts = select(u for u in User if u in event.signouts).order_by(User.character)    
                        for user in signouts:
                            msg = msg + "[{}]({})<{}><{}/{}>\n".format(user.character, user.gameclass.capitalize(), user.lvl, (user.ap + user.awap) / 2 + user.dp, user.axe)
                        msg = msg + "\n```\n"
                        
                        message = await self.bot.send_message(channel, msg)
                        print('Display added: ' + message.id)
                        display = Display(event_id=event_id, message_id=message.id)
                        commit()


    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def clear(self, ctx):
        async for x in self.bot.logs_from(ctx.message.channel):
            await self.bot.delete_message(x)
            await asyncio.sleep(0.3)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def update(self, ctx):
        Popen(static.update_script, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


startup_extensions = ["Luxsheet", "Dbadmin"]

bot = Bot(description="Node War Bot by Agor&Blue&Yuyu", command_prefix=secret.COMMAND_PREFIX, pm_help=False)
Kujibot = Kujibot(bot)
bot.add_cog(Kujibot) 

@bot.event
async def on_ready():
    print('Logged in as '+bot.user.name+' (ID:'+bot.user.id+') | Connected to '+str(len(bot.servers))+' servers | Connected to '+str(len(set(bot.get_all_members())))+' users')
    print('Bot Version: {}'.format(static.version))
    print('--------')
    print('Use this link to invite {}:'.format(bot.user.name))
    print('https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=8'.format(bot.user.id))

    channel = bot.get_channel(secret.channel_status) 
    if channel:       
        print('Clearing '+channel.name+' and adding event')
        
        async for x in bot.logs_from(channel):
            await bot.delete_message(x)
            await asyncio.sleep(0.3)
        
        await asyncio.sleep(1)

        event_id = False
        with db_session:
            displays = select(d for d in Display)
            for d in displays:
                event_id=d.event_id

            delete(d for d in Display)
            commit()

            if event_id:
                await Kujibot.update_display(event_id)

    ready_channel = bot.get_channel(secret.channel_main) 
    await bot.send_message(ready_channel, "Hello, LuxBot v{} is here!".format(static.version))

@bot.event
async def on_message(message):
    if message.channel.id == secret.channel_status:
        static.mute = True
    await bot.process_commands(message)
    static.mute = False

    if message.channel.id == secret.channel_status:
        await asyncio.sleep(0.1)
        print('Incoming message in eventchan: ' + message.id)

        with db_session:
            displays = select(d for d in Display)
            
            if ((secret.role_admin or secret.role_commander) not in [role.name for role in message.author.roles]) and (message.id not in [display.message_id for display in displays]):
                await bot.delete_message(message)
                print('Message deleted: {}'.format(message.id))

            if ((secret.role_admin or secret.role_commander) in [role.name for role in message.author.roles]) and (message.content.startswith(secret.COMMAND_PREFIX)):
                await bot.delete_message(message)
                print('Message deleted: {}'.format(message.id))

def main():
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension {}\n{}'.format(extension, exc))

    bot.run(secret.BOT_API_KEY)


if __name__ == "__main__":
    main()
