import discord
from discord.ext.commands import Bot
from discord.ext import commands

from SheetService import *
from Database import *

class Luxsheet(object):
    def __init__(self, bot):
        self.bot = bot
        self.sheetservice = SheetService()

    def usage_character(self):
        embed = discord.Embed(title="Usage: {}character Name Class Level AP AWAP DP Axe".format(secret.COMMAND_PREFIX), description="", color=0xffff00)
        embed.add_field(name="Classes", value=" ".join(static.classes), inline=False)
        embed.add_field(name="Axe levels", value=" ".join(static.enhance_levels), inline=False)

        return embed

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def sheetlist(self, ctx):
        names = self.sheetservice.get_signups()
        
        if not names:
            msg = "No data found."
        else:
            msg = "Names: \n```md\n"
            msg = msg + "\n".join("{}".format(row[0]) for row in names)
            msg = msg + "```"

        if not static.mute:
            await self.bot.say(msg)
        
    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def sheetclear(self, ctx):
        self.sheetservice.clear()
        if not static.mute:
            await self.bot.say("{}, sheet characters have been cleared".format(ctx.message.author.mention))

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def sheetexport(self, ctx, event_id:int):
        with db_session:
            event = Event.get(id=event_id)
            if not event:
                if not static.mute:
                    await self.bot.say("{}, that event does not exist.".format(ctx.message.author.mention))
                return

            self.sheetservice.add_signups(event)
            await self.bot.say("{}, sheet characters have been added".format(ctx.message.author.mention))

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_member)
    async def character(self, ctx, *args):  
        num_args = len(args)

        if num_args != 7:
            embed = self.usage_character()
            if not static.mute:
                await self.bot.send_message(ctx.message.channel, embed=embed)
        else:
            name, cl, lvl, ap, awap, dp, axe = args

            with db_session:
                user = get_user(ctx.message.author.id)

                if not user.family:
                    if not static.mute:
                        await self.bot.say("{}, please add your family name first: {}family".format(ctx.message.author.mention, secret.COMMAND_PREFIX))
                    return

                if cl.lower() not in static.classes or axe not in static.enhance_levels:
                    embed = self.usage_character()
                    if not static.mute:
                        await self.bot.send_message(ctx.message.channel, embed=embed)
                    return

                user.ap = ap
                user.awap = awap
                user.dp = dp
                user.lvl = lvl
                user.character = name
                user.gameclass = cl.lower()
                user.axe = axe
                commit()

                if not static.mute:
                    await self.bot.say("{}, your character has been added.".format(ctx.message.author.mention))

                self.sheetservice.update_user(user, ctx.message.author.name)

    @commands.command(pass_context=True, no_pm=True)
    @commands.has_role(secret.role_admin)
    async def editcharacter(self, ctx, *args):  
        num_args = len(args)

        if num_args != 8:
            embed = self.usage_character()
            if not static.mute:
                await self.bot.send_message(ctx.message.channel, embed=embed)
        else:
            discord_id, name, cl, lvl, ap, awap, dp, axe = args

            with db_session:
                user = get_user(discord_id)

                if not user.family:
                    if not static.mute:
                        await self.bot.say("{}, {} needs to add a family first.".format(ctx.message.author.mention, discord_id))
                    return

                if cl.lower() not in static.classes or axe not in static.enhance_levels:
                    embed = self.usage_character()
                    if not static.mute:
                        await self.bot.send_message(ctx.message.channel, embed=embed)
                    return

                user.ap = ap
                user.awap = awap
                user.dp = dp
                user.lvl = lvl
                user.character = name
                user.gameclass = cl.lower()
                user.axe = axe
                commit()

                if not static.mute:
                    await self.bot.say("{}, the character has been edited.".format(ctx.message.author.mention))

                self.sheetservice.update_user(user, "ADMINEDIT")


def setup(bot):
    bot.add_cog(Luxsheet(bot))