import pygsheets
import static
import math

from Database import *

class SheetService(object):
    def __init__(self):
        self.client = pygsheets.authorize(service_file=secret.service_account_file)

        self.war_sheet = self.client.open_by_key(secret.war_sheet_id)
        self.war_worksheet = self.war_sheet.worksheet_by_title(static.war_sheet_name)

        self.member_sheet = self.client.open_by_key(secret.member_sheet_id)
        self.member_worksheet = self.member_sheet.worksheet_by_title(static.member_sheet_name)

    def get_signups(self):
        start = "{}{}".format(static.war_sheet_start_column, static.war_sheet_start_row)
        end = "{}{}".format(static.war_sheet_end_column, static.war_sheet_end_row)
        result = self.war_worksheet.get_values(start=start, end=end)

        return result


    def add_signups(self, event:Event):
        self.clear()

        start = "{}{}".format(static.war_sheet_start_column, static.war_sheet_start_row)
        end = "{}{}".format(static.war_sheet_end_column, static.war_sheet_end_row)       

        values = []

        if len(event.signups):
            i = static.war_sheet_start_row
            for user in event.signups:
                row = []
                row.append(user.character)
                row.append(user.lvl)
                row.append(user.ap)
                row.append(user.awap)
                row.append(user.dp)
                formula = static.war_sheet_d_column_formula.replace('XXX', str(i))
                row.append(formula)
                if(user in event.late):
                    row.append("L")
                values.append(row)
                i = i + 1

        self.war_worksheet.append_table(start=start, end=end, values=values, overwrite=True)
        
    def remove_signup(self, user:User):
        name = user.character
        result = self.war_worksheet.find(query=name)

        if len(result):
            for row in result:
                # Tuple Format (row,column)
                after_start = (row.row+1,row.col)
                after_end = (100,row.col)
                
                self.war_worksheet.update_cell(row.label, "")

                names_after = self.war_worksheet.get_values(start=after_start, end=after_end)
                self.war_worksheet.clear(start=after_start, end=after_end)

                self.war_worksheet.update_cells(crange=row.label,values=names_after)

    def clear(self):
        label_start = "{}{}".format(static.war_sheet_start_column, static.war_sheet_start_row)
        label_end = "{}{}".format(static.war_sheet_end_column, static.war_sheet_end_row)
        self.war_worksheet.clear(start=label_start, end=label_end)
        
    def update_user(self, user:User, discord_name:str):
        result = self.member_worksheet.find(query=user.family)

        if len(result):
            for row in result:                
                """ Character """
                self.member_worksheet.update_cell("{}{}".format(static.member_sheet_character, row.row), user.character)
                """ Class """
                self.member_worksheet.update_cell("{}{}".format(static.member_sheet_class, row.row), user.gameclass.capitalize())
                """ Lvl """
                self.member_worksheet.update_cell("{}{}".format(static.member_sheet_lvl, row.row), user.lvl)
                
                if discord_name != "ADMINEDIT":
                    """ Discord """
                    self.member_worksheet.update_cell("{}{}".format(static.member_sheet_discord, row.row), discord_name)
                    """ Discord Id """                    
                    self.member_worksheet.update_cell("{}{}".format(static.member_sheet_discordid, row.row), user.discord_id)

                """ Gearscore """
                self.member_worksheet.update_cell("{}{}".format(static.member_sheet_gear, row.row), ((user.ap + user.awap) / 2 + user.dp))
                """ Axe """
                self.member_worksheet.update_cell("{}{}".format(static.member_sheet_axe, row.row), user.axe)