from datetime import datetime
from pony.orm import Database, Required, PrimaryKey, Set, db_session, commit, Optional
import secret

db = Database()

class User(db.Entity):
    discord_id = PrimaryKey(str)
    family = Optional(str)
    events = Set("Event", reverse="creator")
    signups = Set("Event", reverse="signups")
    signouts = Set("Event", reverse="signouts")
    late = Set("Event", reverse="late")
    ap = Optional(int)
    awap = Optional(int)
    dp = Optional(int)
    lvl = Optional(int)
    character = Optional(str)
    gameclass = Optional(str)
    axe = Optional(str)

class Event(db.Entity):
    time = Required(datetime)
    updated = Required(datetime)
    title = Required(str)
    description = Required(str)
    creator = Required(User, reverse="events")
    signups = Set(User, reverse="signups")
    signouts = Set(User, reverse="signouts")
    late = Set(User, reverse="late")

class Display(db.Entity):
    event_id = Required(int)
    message_id = Required(str)

db.bind(provider="sqlite", filename=secret.DB_PATH, create_db=True)
db.generate_mapping(create_tables=True)

@db_session
def get_user_or_create(discord_id):
    """Returns a User identified by his discord ID, or creates a new one."""
    u = User.get(discord_id=discord_id)
    if not u:
        u = User(discord_id=discord_id)
        commit()
    return u

@db_session
def get_user(discord_id):
    """Returns User identified by his discord ID if a character exists."""
    u = User.get(discord_id=discord_id)
    return u